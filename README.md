SciQtWidgets
===========================

SciQtWidgets is a collections of pyQt5 widgets for use in scientific software.

## Install

SciQtWidgets doesn't yet install via pip or conda. Currently I install SciQtWidgets by cloning the repository into a directory on the python search path. You can check the python search path in the python console with:

    import sys
    print(sys.path)
    
In conda you can add a directory to the python search path with:
 
     conda develop "/path/I/want/to/add"
     
### Install SciQtWidgets into QtDesigner:

To use SciQtWidgets in QtDesigner you need the standalone QtDesigner application, with python plugins enabled. Anaconda comes bundled with the correct up to date QtDesigner.

Go to the SciQtWidgets directory and run the script `installToDesigner.py`, this requires PyQt5 to be installed. Now open QtDesigner and SciQtWidgets should be available![QtDesigner Screenshot](Doc/Images/DeisgnerScreenshot.png) 