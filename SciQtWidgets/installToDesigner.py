#!/usr/bin/env python
try:
    from PyQt5.QtCore import QLibraryInfo
except ImportError:
    print("Cannot install SciQtWidgets without PyQt5")

import os

here = os.path.abspath(os.path.dirname(__file__))
os.chdir(here)
qtpluginpath = QLibraryInfo.location(QLibraryInfo.PluginsPath)
pythonpluginpath = os.path.join(unicode(qtpluginpath), "designer", "python")
if not os.path.exists(pythonpluginpath):
    os.mkdir(pythonpluginpath)

pluginfile = "SciQtWidgetsplugin.py"
print("Copying %s to %s"%(pluginfile,pythonpluginpath))
open(os.path.join(pythonpluginpath, pluginfile), "wb").write(open(pluginfile, "rb").read())

icondir = "SciQtWidgetIcons"
iconpath = os.path.join(pythonpluginpath, icondir)
if not os.path.exists(iconpath):
    os.mkdir(iconpath)

for icon in os.listdir(icondir):
    print("Copying %s to %s"%(icon,iconpath))
    open(os.path.join(iconpath, icon), "wb").write(open(os.path.join(icondir,icon), "rb").read())