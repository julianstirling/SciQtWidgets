from PyQt5 import QtWidgets,QtGui
from PyQt5.QtCore import pyqtSlot,pyqtProperty,pyqtSignal,Qt,QSize
import math

class zeroSizeWidget(QtWidgets.QWidget):
    def __init__(self,parent = None):
        QtWidgets.QWidget.__init__(self, parent)
        self.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Ignored)
        
    def sizeHint(self):
        return QSize(0,0)

class SciQtWidget(QtWidgets.QWidget):
    
    def __init__(self,parent = None):
        QtWidgets.QWidget.__init__(self, parent)
        self.setSizePolicy(QtWidgets.QSizePolicy.Fixed,QtWidgets.QSizePolicy.Fixed)
        self._labelPosition = QtWidgets.QTabWidget.North
        self._labelText="Label"
        self.label = QtWidgets.QLabel(self._labelText)
        self._CoreWidget = QtWidgets.QWidget()
        self._zw1 = zeroSizeWidget()
        self._zw2 = zeroSizeWidget()
        layout = QtWidgets.QGridLayout(self)
        self.label.setSizePolicy(QtWidgets.QSizePolicy.Preferred,QtWidgets.QSizePolicy.Maximum)
        layout.addWidget(self.label, 0, 0)
        layout.setContentsMargins(0,0,0,0,)
        layout.setVerticalSpacing(0)
        layout.setHorizontalSpacing(0)
        
        
        
    def _AddCoreWidget(self,widget):
        self._CoreWidget = widget
        self.setLabelPosition(self._labelPosition,first=True)
        return self._CoreWidget
    
    @pyqtSlot(str)
    def setLabelText(self,text):
        self._labelText=text
        self.label.setText(self._labelText)
        
    def getLabelText(self):
        return self._labelText
    
    @pyqtSlot(QtWidgets.QTabWidget.TabPosition)
    def setLabelPosition(self,Pos,first=False):
        #This bellow is ungly as sin and needs pettying upp but is currently out in full
        #because it is easier to edit. Qt's way of deciding sizes is rather confusing
        #and I can find a way from changing a 2x1 grid to a 1x2, so I create a 2x2 and then
        #set zero size widgets into the two "empty" squares. However on the first creation
        #of the widget can't use size Minium as the initial size takes the value of the child
        #widgets and balloons. The child widgets seem to need to be set on such a high number
        #to always dominate stretching. There probably is an elagent way to do the Qlayout but
        #I cannot find it!
        self._labelPosition = Pos
        if Pos == QtWidgets.QTabWidget.North:
            self.layout().addWidget(self.label, 0, 0)
            self.layout().addWidget(self._CoreWidget, 1, 0)
            if not first:
                self.layout().addWidget(self._zw1,0,1)
                self.layout().addWidget(self._zw2, 1, 1)
                self._CoreWidget.setSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum)
                self.label.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Maximum)
            else:
                self._CoreWidget.setSizePolicy(QtWidgets.QSizePolicy.Ignored,QtWidgets.QSizePolicy.Ignored)
                self.label.setSizePolicy(QtWidgets.QSizePolicy.Preferred,QtWidgets.QSizePolicy.Maximum)
        elif Pos == QtWidgets.QTabWidget.East:
            self.layout().addWidget(self.label, 0, 1)
            self.layout().addWidget(self._CoreWidget, 0, 0)
            if not first:
                self.layout().addWidget(self._zw1,1,0)
                self.layout().addWidget(self._zw2, 1, 1)
                self._CoreWidget.setSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum)
                self.label.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Maximum)
            else:
                self._CoreWidget.setSizePolicy(QtWidgets.QSizePolicy.Ignored,QtWidgets.QSizePolicy.Ignored)
                self.label.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Preferred)
        elif Pos == QtWidgets.QTabWidget.South:
            self.layout().addWidget(self.label, 1, 0)
            self.layout().addWidget(self._CoreWidget, 0, 0)
            if not first:
                self.layout().addWidget(self._zw1,0,1)
                self.layout().addWidget(self._zw2, 1, 1)
                self._CoreWidget.setSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum)
                self.label.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Maximum)
            else:
                self._CoreWidget.setSizePolicy(QtWidgets.QSizePolicy.Ignored,QtWidgets.QSizePolicy.Ignored)
                self.label.setSizePolicy(QtWidgets.QSizePolicy.Preferred,QtWidgets.QSizePolicy.Maximum)
        else:
            self.layout().addWidget(self.label, 0, 0)
            self.layout().addWidget(self._CoreWidget, 0, 1)
            if not first:
                self.layout().addWidget(self._zw1,1,0)
                self.layout().addWidget(self._zw2, 1, 1)
                self._CoreWidget.setSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum)
                self.label.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Maximum)
            else:
                self._CoreWidget.setSizePolicy(QtWidgets.QSizePolicy.Ignored,QtWidgets.QSizePolicy.Ignored)
                self.label.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Preferred)
        
    def getLabelPosition(self):
        return self._labelPosition
    
    labelText = pyqtProperty(str,getLabelText,setLabelText)
    labelPosition = pyqtProperty(QtWidgets.QTabWidget.TabPosition,getLabelPosition,setLabelPosition)

class LEDCore(QtWidgets.QCheckBox):
    GREEN=0
    RED=1
    BLUE=2
    col=0
     
    def __init__(self,parent=None,color=QtGui.QColor('Green')):
        QtWidgets.QCheckBox.__init__(self,parent)
        self._color=color
        self._C1 = QtGui.QColor()
        self._C2 = QtGui.QColor()
        self._C3 = QtGui.QColor()
        self._C4 = QtGui.QColor()
        self._C5 = QtGui.QColor()
        self._C6 = QtGui.QColor()
        self._w = 0
        self.setStyle(color=color)
        self.setEnabled(False)
    
    def paintEvent(self, event):
        if not self._w == min([self.geometry().width(),self.geometry().height()]):
            self.setStyle()
        QtWidgets.QCheckBox.paintEvent(self,event)
    
    def setEnabled(self,state):
        QtWidgets.QCheckBox.setEnabled(self,False)
    
    def resize(self,*args):
        QtWidgets.QCheckBox.resize(self,*args)
        self.setStyle()
    
    def setStyle(self,color=None):
        updated=False
        if not color is None:
            updated=True
            self._color = color
            H,S,V,alpha = color.getHsv()
            self._C1.setHsv(H, 223, 255, 255)
            self._C2.setHsv(H, 218, 221, 255)
            self._C3.setHsv(H, 215, 204, 255)
            self._C4.setHsv(H, 255, 170, 255)
            self._C5.setHsv(H, 255, 136, 255)
            self._C6.setHsv(H, 255, 102, 255)
            
        self._w = min([self.geometry().width(),self.geometry().height()])
        r = int(math.floor(self._w/2))
        
        self.setStyleSheet("QCheckBox::indicator:checked{background-color: qradialgradient(cx: .4, cy: .4, radius: .5, fx: .4, fy: .4, stop: 0 %s, stop: 0.4 %s, stop: 0.8 %s);"%(self._C1.name(),self._C2.name(),self._C3.name())+
            "width: %dpx;"%(self._w-4)+
            "height: %dpx;"%(self._w-4)+
            "border-width: 2px;"+
            "border-color: #000;"+
            "border-style: solid;"+
            "border-radius: %dpx;"%(r)+
            "padding: 0px;}"
            "QCheckBox::indicator:unchecked{background-color: qradialgradient(cx: .4, cy: .4, radius: .5, fx: .4, fy: .4, stop: 0%s, stop: 0.4 %s, stop: 0.8 %s);"%(self._C4.name(),self._C5.name(),self._C6.name())+   
            "width: %dpx;"%(self._w-4)+
            "height: %dpx;"%(self._w-4)+
            "border-width: 2px;"+
            "border-color: #000;"+
            "border-style: solid;"+
            "border-radius: %dpx;"%(r)+
            "padding: 0px;}")
    
    def sizeHint(self):
        return QSize(16777215,16777215)

class LED(SciQtWidget):
    
    stateChanged = pyqtSignal(float)
    
    def __init__(self,parent = None):
        SciQtWidget.__init__(self, parent)
        self._color = QtGui.QColor('Green')
        self.LED = self._AddCoreWidget(LEDCore())
        self.LED.stateChanged.connect(self._stateChanged)
        
    def sizeHint(self):
        return QSize(61,41)
    
    def _stateChanged(self,val):
        self.stateChanged.emit(bool(val))
    
    @pyqtSlot(bool)
    def setOn(self,state):
        self.LED.setChecked(state)
    
    def isOn(self):
        return self.LED.isChecked()
    
    @pyqtSlot(bool)
    def setColor(self,Color):
        self._color=QtGui.QColor(Color)
        self.LED.setStyle(self._color)
    
    def getColor(self):
        return self._color
    
    On = pyqtProperty(bool,isOn,setOn)
    color = pyqtProperty(QtGui.QColor,getColor,setColor)

class floatInputCore(QtWidgets.QAbstractSpinBox):

    def __init__(self,SciQtParent,*args):
        self.parent = SciQtParent
        self._type=float
        self._value=0.0
        self._stepsize=0.1
        self._minimum = -float("Inf")
        self._maximum  = float("Inf")
        self._formatstring = "%#0.3g"
        self._iscontrol = True
        self._keyStepEn = True
        self._scrollStepEn = True
        QtWidgets.QAbstractSpinBox.__init__(self,*args)
        self.setCorrectionMode(self.CorrectToNearestValue)
        self.setButtonSymbols(self.UpDownArrows)
        self.editingFinished.connect(self.interpretText)
        self.updateText()
        
    def sizeHint(self):
        return QSize(16777215,16777215)
    
    def interpretText(self): 
        try:
            value=self._type(self.text())
            if not value == self._value:
                self.setValue(value)
        except ValueError:
            pass
    
    def updateText(self):
        try:
            self.lineEdit().setText(self._formatstring%self._value)
        except ValueError:
            self.lineEdit().setText("Format Err!")
    
    
    def keyPressEvent(self,event):
        if event.key() == Qt.Key_Escape:
            self.updateText()
        if not self.isKeyStepEnabled():
            if (event.key() == Qt.Key_PageUp) or (event.key() == Qt.Key_PageDown) or (event.key() == Qt.Key_Up) or (event.key() == Qt.Key_Down):
                return
        QtWidgets.QAbstractSpinBox.keyPressEvent(self,event)
    
    def wheelEvent(self,event):
        if self.isScrollStepEnabled:
            QtWidgets.QAbstractSpinBox.wheelEvent(self,event)
    
    def setFormatString(self,string):
        if isinstance(string, basestring):
            self._formatstring = string
        else:
            raise TypeError('Format must be a string.')
        self.updateText()
    
    def getFormatString(self):
        return self._formatstring
    
    def setValue(self,number):
        if isinstance(number, self._type):
            in_value = number
        else:
            try:
                in_value=self._type(number)
            except ValueError:
                pass
        if (in_value<=self._maximum) and (in_value>=self._minimum):
            self._value=in_value;
        else:
            if self.correctionMode() == self.CorrectToNearestValue:
                if (in_value>self._maximum):
                    self._value=self._maximum
                else:
                    self._value=self._minimum
            
        self.parent.valueChanged.emit(self._value)
        self.updateText()
    
    def getValue(self):
        return self._value
    
    def setStepSize(self,number):
        if not isinstance(number, self._type):
            raise TypeError('Step size should be a floating point number')
        self._stepsize = abs(number)
        
    def getStepSize(self):
        return self._stepsize
    
    def setMaximum(self,number):
        if not isinstance(number, self._type):
            raise TypeError('Maximum should be a floating point number')
        if number>self._minimum:
            self._maximum = number
        else:
            self._maximum = self._minimum
       
        
    def getMaximum(self):
        return self._maximum
    
    def setMinimum(self,number):
        if not isinstance(number, self._type):
            raise TypeError('Maximum should be a floating point number')
        if number<self._maximum:
            self._minimum = number
        else:
            self._minimum = self._maximum
    
    def getMinimum(self):
        return self._minimum
    
    def setIsControl(self,isCtl):
        self._iscontrol=bool(isCtl)
        if self._iscontrol:
            self.setButtonSymbols(self.UpDownArrows)
            self.setReadOnly(False)
        else:
            self.setButtonSymbols(self.NoButtons)
            self.setReadOnly(True)
    
    def isControl(self):
        return self._iscontrol
    
    def setKeyStepEnabled(self,En):
        self._keyStepEn = En
    
    def isKeyStepEnabled(self):
        return self._keyStepEn
    
    def setScrollStepEnabled(self,En):
        self._scrollStepEn = En
    
    def isScrollStepEnabled(self):
        return self._scrollStepEn
    
    def validate(self,string,pos):
        try:
            self._type(string)
            valid = QtGui.QValidator.Acceptable
        except ValueError:
            valid  = QtGui.QValidator.Intermediate
        return (valid,string,pos)
    
    def stepEnabled(self):
        if self.isReadOnly():
            return self.StepNone
        if self._value<self._maximum:
            exUp = self.StepUpEnabled
        else:
            exUp = self.StepNone
        if self._value>self._minimum:
            exDown = self.StepDownEnabled
        else:
            exDown = self.StepNone
        return exUp|exDown
    
    def stepBy(self,steps):
        self.setValue(self._value+steps*self._stepsize)
    

class floatInput(SciQtWidget):
    
    valueChanged = pyqtSignal(float)
    
    def __init__(self,parent = None):
        SciQtWidget.__init__(self, parent)
        self.Input = self._AddCoreWidget(floatInputCore(self))
        
    def sizeHint(self):
        return QSize(61,41)
    
    @pyqtSlot(str)
    def setFormatString(self,string):
        self.Input.setFormatString(string)
    
    def getFormatString(self):
        return self.Input.getFormatString()
    
    @pyqtSlot(float)
    def setValue(self,number):
        self.Input.setValue(number)
    
    def getValue(self):
        return self.Input.getValue()
    
    @pyqtSlot(float)
    def setStepSize(self,number):
        self.Input.setStepSize(number)
        
    def getStepSize(self):
        return self.Input.getStepSize()
    
    @pyqtSlot(float)
    def setMaximum(self,number):
        self.Input.setMaximum(number)
        
    def getMaximum(self):
        return self.Input.getMaximum()
    
    @pyqtSlot(float)
    def setMinimum(self,number):
        self.Input.setMinimum(number)
    
    def getMinimum(self):
        return self.Input.getMinimum()
    
    @pyqtSlot(bool)
    def setIsControl(self,isCtl):
        self.Input.setIsControl(isCtl)
    
    def isControl(self):
        return self.Input.isControl()
    
    @pyqtSlot(bool)
    def setKeyStepEnabled(self,En):
        self.Input.setKeyStepEnabled(En)
    
    def isKeyStepEnabled(self):
        return self.Input.isKeyStepEnabled()
    
    @pyqtSlot(bool)
    def setScrollStepEnabled(self,En):
        self.Input.setScrollStepEnabled(En)
    
    def isScrollStepEnabled(self):
        return self.Input.isScrollStepEnabled()
    
    formatString = pyqtProperty(str, getFormatString, setFormatString)
    value = pyqtProperty(float, getValue, setValue)
    stepSize = pyqtProperty(float, getStepSize, setStepSize)
    maximum = pyqtProperty(float, getMaximum, setMaximum)
    minimum = pyqtProperty(float, getMinimum, setMinimum)
    isControl = pyqtProperty(bool, isControl,setIsControl)
    enableKeyboardStep = pyqtProperty(bool, isKeyStepEnabled,setKeyStepEnabled)
    enableScrollStep = pyqtProperty(bool, isScrollStepEnabled,setScrollStepEnabled)