
from PyQt5.QtGui import QIcon
from PyQt5.QtDesigner import QPyDesignerCustomWidgetPlugin
from os import path
from SciQtWidgets import LED,floatInput
from pyqtgraph import PlotWidget


class LEDPlugin(QPyDesignerCustomWidgetPlugin):

    def __init__(self, parent=None):
        super(LEDPlugin, self).__init__(parent)
        self.initialized = False

    def initialize(self, core):
        if self.initialized:
            return
        self.initialized = True

    def isInitialized(self):
        return self.initialized

    def createWidget(self, parent):
        return LED(parent)

    def name(self):
        return "LED"

    def group(self):
        return "SciQtWidgets"

    def icon(self):
        here = path.abspath(path.dirname(__file__))
        return QIcon(path.join(here,"SciQtWidgetIcons","LED.png"))

    def toolTip(self):
        return "An LED widget"

    def whatsThis(self):
        return "An LED widget"

    def isContainer(self):
        return False

    def includeFile(self):
        return "SciQtWidgets"
    
class floatInputPlugin(QPyDesignerCustomWidgetPlugin):

    def __init__(self, parent=None):
        super(floatInputPlugin, self).__init__(parent)
        self.initialized = False

    def initialize(self, core):
        if self.initialized:
            return
        self.initialized = True

    def isInitialized(self):
        return self.initialized

    def createWidget(self, parent):
        return floatInput(parent)

    def name(self):
        return "floatInput"

    def group(self):
        return "SciQtWidgets"

    def icon(self):
        here = path.abspath(path.dirname(__file__))
        return QIcon(path.join(here,"SciQtWidgetIcons","floatInput.png"))

    def toolTip(self):
        return "Parsed input for floating point data"

    def whatsThis(self):
        return "Parsed input for floating point data"

    def isContainer(self):
        return False

    def includeFile(self):
        return "SciQtWidgets"

class PlotWidgetPlugin(QPyDesignerCustomWidgetPlugin):

    def __init__(self, parent=None):
        super(PlotWidgetPlugin, self).__init__(parent)
        self.initialized = False

    def initialize(self, core):
        if self.initialized:
            return
        self.initialized = True

    def isInitialized(self):
        return self.initialized

    def createWidget(self, parent):
        return PlotWidget(parent)

    def name(self):
        return "PlotWidget"

    def group(self):
        return "SciQtWidgets"

    def icon(self):
        return QIcon()

    def toolTip(self):
        return "Plot widget"

    def whatsThis(self):
        return "Plot widget"

    def isContainer(self):
        return False

    def includeFile(self):
        return "pyqtgraph"
    
